Soho lets you build a web site from a set of reStructuredText source
files (the content) and a template file (the layout).

See ``doc/README.txt`` for further details.
